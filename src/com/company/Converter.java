package com.company;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Scanner;

public class Converter {
    private Scanner in;
    private JFrame jFrame;
    private JPanel jPanel;
    private JButton convert;
    private Font font;

    private JTextField inputFrom;
    private JTextField inputTo;

    private JComboBox cbFrom;
    private JComboBox cbTo;

    private final String[] modes = {"Length", "Temperature", "Weight", "Time"};
    private final String[] lengthModeFrom = {"Meters", "Yards", "Miles"};
    private final String[] lengthModeTo = {"Meters", "Yards", "Miles"};
    private final String[] temperatureModeFrom = {"Celsius", "Fahrenheit", "Kelvin"};
    private final String[] temperatureModeTo = {"Celsius", "Fahrenheit", "Kelvin"};
    private final String[] weightModeFrom = {"KG", "LBS", "ST"};
    private final String[] weightModeTo = {"KG", "LBS", "ST"};
    private final String[] timeModeFrom = {"Second", "Minute", "Hour"};
    private final String[] timeModeTo = {"Second", "Minute", "Hour"};

    public Converter() {
        in = new Scanner(System.in);
        jFrame = new JFrame();
        jPanel = new JPanel();
        font = new Font("", Font.ROMAN_BASELINE, 16);
        constructFrame();
    }

    private void constructFrame() {
        Toolkit toolkit = Toolkit.getDefaultToolkit(); //Подключаем класс Toolkit
        Dimension dimension = toolkit.getScreenSize(); //Получаем разрешение нашего экрана от Toolkit
        jFrame.setTitle("Converter"); //Название программы
        jFrame.setBounds(dimension.width / 2 - 200, dimension.height / 2 - 200, 460, 220); //Установка высоты, ширины, отступа от угла экрана
        jFrame.getDefaultCloseOperation(); //Возможность закрытия приложения
        jFrame.setVisible(true); //Отображение программы
        jFrame.add(jPanel); //Добавление в флейм панели
        constructPanel();
    }


    void constructPanel() {
        jPanel.setLayout(null); //Стандартное позиционирование

        //Поле ввода1
        inputFrom = new JTextField(23); //Поле ввода
        inputFrom.setBounds(20, 60, 190, 30);
        inputFrom.setFont(font); //Присвоить шрифт
        jPanel.add(inputFrom);

        //Поле ввода2
        inputTo = new JTextField(23); //Поле ввода
        inputTo.setBounds(250, 60, 190, 30);
        inputTo.setFont(font); //Присвоить шрифт
        jPanel.add(inputTo);

        //Кнопка
        convert = new JButton("Convert");
        convert.setBounds(180, 130, 100, 30);
        convert.setFont(font);
        convert.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String str1 = (String) cbFrom.getSelectedItem();
                String str2 = (String) cbTo.getSelectedItem();
                String coef = getCoefficientByMode((str1 + str2).toLowerCase());
            }
        });
        jPanel.add(convert); //Добавление к панели кнопки

        JComboBox cbModes = new JComboBox(modes);
        cbModes.setBounds(175, 17, 110, 30);
        cbModes.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent event) {
                if (event.getStateChange() == ItemEvent.SELECTED) {
                    String selectedMode = (String) event.getItem();
                    switch (selectedMode) {
                        case "Length":
                            createComboBoxes(lengthModeFrom, lengthModeTo, true);
                            break;
                        case "Temperature":
                            createComboBoxes(temperatureModeFrom, temperatureModeTo, true);
                            break;
                        case "Weight":
                            createComboBoxes(weightModeFrom, weightModeTo, true);
                            break;
                        case "Time":
                            createComboBoxes(timeModeFrom, timeModeTo, true);
                            break;
                    }
                }
            }
        });
        jPanel.add(cbModes);

        createComboBoxes(lengthModeFrom, lengthModeTo, false);
    }

    void createComboBoxes(String[] settingsFrom, String[] settingsTo, boolean isRecreate) {
        if (isRecreate) {
            jPanel.remove(cbFrom);
            jPanel.remove(cbTo);
        }

        cbFrom = new JComboBox(settingsFrom);
        cbFrom.setBounds(20, 87, 190, 30);
        jPanel.add(cbFrom);

        cbTo = new JComboBox(settingsTo);
        cbTo.setBounds(250, 87, 190, 30);
        jPanel.add(cbTo);
    }

    private String getCoefficientByMode(String mode) {
        double coefficient = 0;
        switch (mode) {
            case "metersmeters":
                inputTo.setText(inputFrom.getText());
                break;
            case "metersyards":
                double convertMetersYards = Integer.parseInt(inputFrom.getText());
                inputTo.setText(String.valueOf(convertMetersYards * 1.094));
                break;
            case "metersmiles":
                double convertMetersMiles = Integer.parseInt(inputFrom.getText());
                inputTo.setText(String.valueOf(convertMetersMiles / 1609));
                break;
            case "yardsyards":
                inputTo.setText(inputFrom.getText());
                break;
            case "yardsmiles":
                double convertYardsMiles = Integer.parseInt(inputFrom.getText());
                inputTo.setText(String.valueOf(convertYardsMiles / 1760));
                break;
            case "yardsmeters":
                double convertYardsMeters = Integer.parseInt(inputFrom.getText());
                inputTo.setText(String.valueOf(convertYardsMeters / 1.094));
                break;
            case "milesmiles":
                inputTo.setText(inputFrom.getText());
                break;
            case "milesmeters":
                double convertMilesMeters = Integer.parseInt(inputFrom.getText());
                inputTo.setText(String.valueOf(convertMilesMeters * 1609));
                break;
            case "milesyards":
                double convertMilesYards = Integer.parseInt(inputFrom.getText());
                inputTo.setText(String.valueOf(convertMilesYards * 1760));
                break;
            case "celsiuscelsius":
                inputTo.setText(inputFrom.getText());
                break;
            case "fahrenheitfahrenheit":
                inputTo.setText(inputFrom.getText());
                break;
            case "kelvinkelvin":
                inputTo.setText(inputFrom.getText());
                break;
            case "celsiusfahrenheit":
                double convertconvertCelsiusFahrenheit = Integer.parseInt(inputFrom.getText());
                inputTo.setText(String.valueOf((convertconvertCelsiusFahrenheit * 9 / 5) + 32));
                break;
            case "celsiuskelvin":
                double convertCelsiusKelvin = Integer.parseInt(inputFrom.getText());
                inputTo.setText(String.valueOf(convertCelsiusKelvin + 273.15));
                break;
            case "fahrenheitcelsius":
                double convertFahrenheitCelsius = Integer.parseInt(inputFrom.getText());
                inputTo.setText(String.valueOf((convertFahrenheitCelsius - 32) * 5 / 9));
                break;
            case "fahrenheitkelvin":
                double convertFahrenheitKelvin = Integer.parseInt(inputFrom.getText());
                inputTo.setText(String.valueOf((convertFahrenheitKelvin - 32) * 5 / 9 + 273.15));
                break;
            case "kelvincelsius":
                double convertKelvinCelsius = Integer.parseInt(inputFrom.getText());
                inputTo.setText(String.valueOf((convertKelvinCelsius - 273.15)));
                break;
            case "kelvinfahrenheit":
                double convertKelvinFahrenheit = Integer.parseInt(inputFrom.getText());
                inputTo.setText(String.valueOf((convertKelvinFahrenheit - 273.15) * 9 / 5 + 32));
                break;
            case "kgkg":
                inputTo.setText(inputFrom.getText());
                break;
            case "lbslbs":
                inputTo.setText(inputFrom.getText());
                break;
            case "stst":
                inputTo.setText(inputFrom.getText());
                break;
            case "kglbs":
                double convertKgLbs = Integer.parseInt(inputFrom.getText());
                inputTo.setText(String.valueOf(convertKgLbs * 2.205));
                break;
            case "kgst":
                double convertKgSt = Integer.parseInt(inputFrom.getText());
                inputTo.setText(String.valueOf(convertKgSt / 907));
                break;
            case "lbskg":
                double convertLbsKg = Integer.parseInt(inputFrom.getText());
                inputTo.setText(String.valueOf(convertLbsKg / 2.205));
                break;
            case "lbsst":
                double convertLbsSt = Integer.parseInt(inputFrom.getText());
                inputTo.setText(String.valueOf(convertLbsSt / 2000));
                break;
            case "stkg":
                double convertStKg = Integer.parseInt(inputFrom.getText());
                inputTo.setText(String.valueOf(convertStKg * 907));
                break;
            case "stlbs":
                double convertStLbs = Integer.parseInt(inputFrom.getText());
                inputTo.setText(String.valueOf(convertStLbs * 2000));
                break;
            case "secondsecond":
                inputTo.setText(inputFrom.getText());
                break;
            case "minuteminute":
                inputTo.setText(inputFrom.getText());
                break;
            case "hourhour":
                inputTo.setText(inputFrom.getText());
                break;
            case "secondminute":
                double convertSecondMinute = Integer.parseInt(inputFrom.getText());
                inputTo.setText(String.valueOf(convertSecondMinute / 60));
                break;
            case "secondhour":
                double convertSecondHour = Integer.parseInt(inputFrom.getText());
                inputTo.setText(String.valueOf(convertSecondHour / 3600));
                break;
            case "minutesecond":
                double convertMinuteSecond = Integer.parseInt(inputFrom.getText());
                inputTo.setText(String.valueOf(convertMinuteSecond * 60));
                break;
            case "minutehour":
                double convertMinuteHour = Integer.parseInt(inputFrom.getText());
                inputTo.setText(String.valueOf(convertMinuteHour / 60));
                break;
            case "hoursecond":
                double convertHourSecond = Integer.parseInt(inputFrom.getText());
                inputTo.setText(String.valueOf(convertHourSecond * 3600));
                break;
            case "hourminute":
                double convertHourMinute = Integer.parseInt(inputFrom.getText());
                inputTo.setText(String.valueOf(convertHourMinute * 60));
                break;
        }
        return mode;
    }
}


